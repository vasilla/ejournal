
function deleteItem(userId) {
    let data = {"id":userId};
    makeRequest(JSON.stringify(data),'/admin/edit/',del,json);
}

function updateItem(userId) {
    let id = "edit".concat(userId);
    let email = document.getElementById(id).value;
    let data = {'email':email,"id":userId};
    makeRequest(JSON.stringify(data),'/admin/edit/',update,json);
}

function createItem() {
    let email = document.getElementById('email-new').value;
    let data = {'email':email};
    makeRequest(JSON.stringify(data),'/admin/edit/',post,json);

}
