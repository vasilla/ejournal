
function deleteItem(itemId) {
    let data = {"id":itemId};
    makeRequest(JSON.stringify(data),'/department/edit/',del,json);
}

function updateItem(itemId) {
    let id = "edit".concat(itemId);
    let name = document.getElementById(id).value;
    let data = {'name':name,"id":itemId};
    makeRequest(JSON.stringify(data),'/department/edit/',update,json);
}

function createItem() {
    let name = document.getElementById('newValue').value;
    let data = {'name':name};
    makeRequest(JSON.stringify(data),'/department/edit/',post,json);

}
