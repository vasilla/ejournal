function deleteItem(userId) {
    let data = {"id":userId};
    makeRequest(JSON.stringify(data),'/group/edit/',del,json);
}

function updateItem(userId) {
    let email = document.getElementById("email".concat(userId)).value;
    let subgroup = parseInt(document.getElementById("subgroup".concat(userId)).value);
    let data = {'email':email,"id":userId,'subgroup':subgroup};
    console.log(data);
    makeRequest(JSON.stringify(data),'/group/edit/',update,json);
}

function createItem(superiorId) {
    let email = document.getElementById('email-new').value;
    let subgroup = parseInt(document.getElementById('subgroup-new').value);
    let data = {'email':email,'subgroup':subgroup,'groupId':superiorId};
    console.log(data);
    makeRequest(JSON.stringify(data),'/group/edit/',post,json);

}
