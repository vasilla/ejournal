function deleteItem(id) {
    let data = {"id":id};
    makeRequest(JSON.stringify(data),'/course/edit/',del,json);
}

function updateItem(id) {
    let image = document.getElementById("image".concat(id)).src;
    let name = document.getElementById("name".concat(id)).value;
    let data = {"id":id,'image':image,'name':name};
    makeRequest(JSON.stringify(data),'/course/edit/',update,json);
}

function createItem(superiorId) {
    let image = document.getElementById('image-new').src;
    let name = document.getElementById('name-new').value;
    let data = {'image':image,'name':name,'departmentId':superiorId};
    makeRequest(JSON.stringify(data),'/course/edit/',post,json);

}

function setImage(imgId){
    // console.log(imgId);

    document.getElementById(imgId).src = document.getElementById('image-source').value;

}


$(document).ready(function(){
    $("image-source").imagepicker()
});
