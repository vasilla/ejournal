const post = "POST";
const update = "PATCH";
const del = "DELETE";
const json = "application/json";


function makeRequest(requestBody,requestUrl,requestMethod,contentType) {
    $.ajax({
        url: requestUrl,
        method: requestMethod,
        data: requestBody,
        processData: false,
        contentType: contentType,
        success(response) {
            location.reload();
        },
        error(response) {
            // location.reload();
        }
    });
}