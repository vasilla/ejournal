function deleteItem(userId) {
    let data = {"id":userId};
    makeRequest(JSON.stringify(data),'/professors/edit/',del,json);
}

function updateItem(userId) {
    let email = document.getElementById("edit".concat(userId)).value;
    let admin = document.getElementById("admin".concat(userId)).checked;
    let data = {'email':email,"id":userId,'admin':admin};
    makeRequest(JSON.stringify(data),'/professors/edit/',update,json);
}

function createItem(superiorId) {
    let email = document.getElementById('email-new').value;
    let admin = document.getElementById('isAdmin').checked;
    let data = {'email':email,'admin':admin,'departmentId':superiorId};
    makeRequest(JSON.stringify(data),'/professors/edit/',post,json);

}
