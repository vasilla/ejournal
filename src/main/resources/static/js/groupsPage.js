
function deleteItem(itemId) {
    let data = {"id":itemId};
    makeRequest(JSON.stringify(data),'/groups/edit/',del,json);
}

function updateItem(itemId) {
    let id = "edit".concat(itemId);
    let name = document.getElementById(id).value;
    let data = {'name':name,"id":itemId};
    makeRequest(JSON.stringify(data),'/groups/edit/',update,json);
}

function createItem(superiorId) {
    let name = document.getElementById('newValue').value;
    let data = {'name':name,'departmentId':superiorId};
    makeRequest(JSON.stringify(data),'/groups/edit/',post,json);

}
