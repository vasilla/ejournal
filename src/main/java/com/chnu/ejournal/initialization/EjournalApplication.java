package com.chnu.ejournal.initialization;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.chnu.ejournal")
public class EjournalApplication {

	public static void main(String[] args) {
		SpringApplication.run(EjournalApplication.class, args);
	}
}
