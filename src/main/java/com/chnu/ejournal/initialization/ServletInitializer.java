package com.chnu.ejournal.initialization;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
//		System.out.println("DONE1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"); TODO remove 
		return application.sources(EjournalApplication.class);
	}

}
