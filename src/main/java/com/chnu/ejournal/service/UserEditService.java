package com.chnu.ejournal.service;

import com.chnu.ejournal.dao.*;
import com.chnu.ejournal.exceptions.EmailAlreadyTakenException;
import com.chnu.ejournal.exceptions.InvalidDataException;
import com.chnu.ejournal.exceptions.UserNotExistException;
import com.chnu.ejournal.exceptions.UserRelatedException;
import com.chnu.ejournal.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserEditService {
    @Autowired
    private AdminRepository adminRepository;
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DepartmentsRepository departmentsRepository;
    @Autowired
    private ProfessorRepository professorRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private GroupRepository groupRepository;

    public void createNewAdmin(String email) throws EmailAlreadyTakenException {
        if (userRepository.existsByEmail(email)){
            throw new EmailAlreadyTakenException();
        }
        Admin admin = new Admin();
        admin.setEmail(email);
        admin.setName("unknown");
        admin.setSurname("unknown");
        adminRepository.save(admin);

    }

    public void editEmailOfAdmin(long id, String email) throws UserRelatedException{
        if (!adminRepository.existsById(id)){
            throw new UserNotExistException();
        }
        Optional<Admin> adminOptional = adminRepository.findById(id);
        if (adminOptional.isPresent()){
            Admin admin = adminOptional.get();
            if ((!admin.getEmail().equals(email))&&(userRepository.existsByEmail(email))){
                throw  new EmailAlreadyTakenException();
            }
            admin.setEmail(email);
            adminRepository.save(admin);
        }
        else {
            throw new UserNotExistException();
        }
    }

    public void deleteAdmin(long id) throws UserNotExistException {
        if (!adminRepository.existsById(id)){
            throw new UserNotExistException();
        }
        adminRepository.deleteById(id);
    }

    public void createProfessor(String email, boolean isAdmin, long departmentId)
            throws EmailAlreadyTakenException, InvalidDataException {
        if (userRepository.existsByEmail(email)){
            throw  new EmailAlreadyTakenException();
        }
        Professor professor = new Professor();
        Optional<Department> department = departmentsRepository.findById(departmentId);
        if (department.isPresent()){
            professor.setDepartment(department.get());
            professor.setEmail(email);
            professor.setAdmin(isAdmin);
            professorRepository.save(professor);
        }
        else throw new InvalidDataException("Department id invalid");
    }

    public void deleteProfessor(long id) throws UserNotExistException {
        if (!professorRepository.existsById(id)){
            throw new UserNotExistException();
        }
        professorRepository.deleteById(id);
    }

    public void editProfessor(long id, String email,boolean isAdmin) throws UserRelatedException{
        if (!professorRepository.existsById(id)){
            throw new UserNotExistException();
        }
        Optional<Professor> professorOptional = professorRepository.findById(id);
        if (professorOptional.isPresent()){
            Professor professor = professorOptional.get();
            if ((!professor.getEmail().equals(email))&&(userRepository.existsByEmail(email))){
                throw  new EmailAlreadyTakenException();
            }
            professor.setEmail(email);
            professor.setAdmin(isAdmin);
            professorRepository.save(professor);
        }
        else {
            throw new UserNotExistException();
        }
    }
    public void createStudent(String email, long groupId,int subgroup)
            throws EmailAlreadyTakenException, InvalidDataException {
        if (userRepository.existsByEmail(email)){
            throw  new EmailAlreadyTakenException();
        }
        Optional<Group> groupOptional = groupRepository.findById(groupId);
        if (groupOptional.isPresent()){
            Student student = new Student();
            student.setSubGroup(subgroup);
            student.setParentGroup(groupOptional.get());
            student.setEmail(email);
            studentRepository.save(student);
        }
        else throw new InvalidDataException("Group id invalid");
    }

    public void deleteStudent(long id) throws UserNotExistException {
        if (!studentRepository.existsById(id)){
            throw new UserNotExistException();
        }
        studentRepository.deleteById(id);
    }

    public void editStudent(long id, String email,int subgroup) throws UserRelatedException{
        if (!studentRepository.existsById(id)){
            throw new UserNotExistException();
        }
        Optional<Student> studentOptional = studentRepository.findById(id);
        if (studentOptional.isPresent()){
            Student student = studentOptional.get();
            if ((!student.getEmail().equals(email))&&(userRepository.existsByEmail(email))){
                throw  new EmailAlreadyTakenException();
            }
            student.setEmail(email);
            student.setSubGroup(subgroup);
            studentRepository.save(student);
        }
        else {
            throw new UserNotExistException();
        }
    }

}
