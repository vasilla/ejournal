package com.chnu.ejournal.service;

import com.chnu.ejournal.dao.AdminRepository;
import com.chnu.ejournal.dao.UserRepository;
import com.chnu.ejournal.dto.TokenInfo;
import com.chnu.ejournal.exceptions.TokenInvalidException;
import com.chnu.ejournal.exceptions.UserNotExistException;
import com.chnu.ejournal.model.*;
import com.chnu.ejournal.model.enums.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
public class AuthService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    @Qualifier("googleClientIdForServer")
    private String googleClientIdForServer;

    @Autowired
    @Qualifier("googleClientIdForAndroid1")
    private String googleClientIdForAndroid1;

    @Autowired
    @Qualifier("googleClientIdForAndroid2")
    private String googleClientIdForAndroid2;

    @Autowired
    @Qualifier("googleTokenInfoLink")
    private String googleTokenInfoLink;

    @Autowired
    @Qualifier("defaultAdminEmail")
    private String defaultAdminEmail;

    public boolean isTokenValid(TokenInfo tokenInfo){
        return tokenInfo.getEmailVerified()
                && (tokenInfo.getAzp().equals(googleClientIdForAndroid1)
                || tokenInfo.getAzp().equals(googleClientIdForAndroid2)
                && tokenInfo.getAud().equals(googleClientIdForServer));
    }

    public boolean isTokenValid(OAuth2AuthenticationToken token){
        Map<String, Object> attributes = token.getPrincipal().getAttributes();
        boolean verified = (boolean)attributes.get("email_verified");
        String azp = (String)attributes.get("azp");
        String aud = ((List<String>)attributes.get("aud")).get(0);
        return verified
                && azp.equals(googleClientIdForServer)
                && aud.equals(googleClientIdForServer);

    }






    @Transactional
    public User getUserFromToken(TokenInfo tokenInfo){
        User user = userRepository.getUserByEmail(tokenInfo.getEmail());
        if (user!= null){
            user.setName(tokenInfo.getGivenName());
            user.setSurname(tokenInfo.getFamilyName());
            userRepository.save(user);
        }
        return user;
    }

    public String getGoogleTokenInfoLink(String token){
        return googleTokenInfoLink+token;
    }

    private Role getUserRole(User user){
        if (user instanceof Admin) return Role.ADMIN;
        if (user instanceof Professor) return Role.PROFESSOR;
        return Role.STUDENT;
    }
    private void changeSecurityContextAuth(User user) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        UserPrincipal userPrincipal = new UserPrincipal(user);
        Authentication authentication = new UsernamePasswordAuthenticationToken(userPrincipal,
                null, userPrincipal.getAuthorities());
        securityContext.setAuthentication(authentication);
    }

    public User processToken(Map<String,String> map) throws UserNotExistException, TokenInvalidException {
        TokenInfo tokenInfo = new TokenInfo(map);
        if (!isTokenValid(tokenInfo)){
            throw new TokenInvalidException();
        }

        User user = getUserFromToken(tokenInfo);
        if (user == null){
            throw new UserNotExistException();
        }
        changeSecurityContextAuth(user);
        return user;
    }


    public boolean authenticate(OAuth2AuthenticationToken token) {
       if (!isTokenValid(token)) {
           return false;
       }
       String email = token.getPrincipal().getAttributes().get("email").toString();
       User user = userRepository.getUserByEmail(email);
       if ((user == null)&&(email.equals(defaultAdminEmail))){
           Admin admin = new Admin();
           admin.setEmail(email);
           adminRepository.save(admin);
           changeSecurityContextAuth(admin);
           return true;
       }
       if ((user == null)||(!UserPrincipal.isAdmin(user))){
           return false;
       }
       changeSecurityContextAuth(user);
       return true;


    }
}
