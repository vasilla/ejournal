package com.chnu.ejournal.service;

import com.chnu.ejournal.dao.DepartmentsRepository;
import com.chnu.ejournal.model.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DepartmentService {

    @Autowired
    private DepartmentsRepository departmentsRepository;

    public void updateDepartment(long id,String newName){
        Optional<Department> departmentOptional = departmentsRepository.findById(id);
        if (departmentOptional.isPresent()){
            Department department = departmentOptional.get();
            department.setName(newName);
            departmentsRepository.save(department);
        }
    }

    public void creaateDepartment(String name){
        Department department = new Department();
        department.setName(name);
        departmentsRepository.save(department);
    }

    public void deleteDepartment(long id){
        departmentsRepository.deleteById(id);
    }



}
