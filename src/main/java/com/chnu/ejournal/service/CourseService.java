package com.chnu.ejournal.service;

import com.chnu.ejournal.dao.CourseRepository;
import com.chnu.ejournal.dao.DepartmentsRepository;
import com.chnu.ejournal.exceptions.InvalidDataException;
import com.chnu.ejournal.model.Course;
import com.chnu.ejournal.model.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CourseService {
    @Autowired
    private DepartmentsRepository departmentsRepository;

    @Autowired
    private CourseRepository courseRepository;


    public void createCourse(String name, long imageId, long departmentId)
            throws  InvalidDataException {
        Optional<Department> department = departmentsRepository.findById(departmentId);
        if (department.isPresent()){
            Course course = new Course(name,imageId,department.get());
            courseRepository.save(course);
        }
        else throw new InvalidDataException("Department id invalid");
    }

    public void deleteCourse(long id) throws InvalidDataException {
        if (!courseRepository.existsById(id)){
            throw new InvalidDataException();
        }
        courseRepository.deleteById(id);
    }

    public void editCourse(long id, String name, long imageId) throws InvalidDataException{
        if (!courseRepository.existsById(id)){
            throw new InvalidDataException();
        }
        Optional<Course> courseOptional = courseRepository.findById(id);
        if (courseOptional.isPresent()){
            Course course = courseOptional.get();
            course.setImageId(imageId);
            course.setName(name);
            courseRepository.save(course);
        }
        else {
            throw new InvalidDataException();
        }
    }
}
