package com.chnu.ejournal.service;

import com.chnu.ejournal.dao.DepartmentsRepository;
import com.chnu.ejournal.dao.GroupRepository;
import com.chnu.ejournal.exceptions.InvalidDataException;
import com.chnu.ejournal.model.Department;
import com.chnu.ejournal.model.Group;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class GroupsService {

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private DepartmentsRepository departmentsRepository;

    public void createGroup(String groupName, long departmentId)
            throws  InvalidDataException {

        Group group = new Group();
        Optional<Department> department = departmentsRepository.findById(departmentId);
        if (department.isPresent()){
//            department.get().getGroups().add(group);
            group.setDepartment(department.get()); //TODO check
            group.setName(groupName);
            System.out.println("created !!!!");
            groupRepository.save(group);
        }
        else throw new InvalidDataException("Department id invalid");
    }

    public void deleteGroup(long id) throws InvalidDataException {
        if (!groupRepository.existsById(id)){
            throw new InvalidDataException("Department id not exist");
        }
        groupRepository.deleteById(id);
    }

    public void editGroup(long id, String newName) throws InvalidDataException{
        Optional<Group> groupOptional = groupRepository.findById(id);
        if (groupOptional.isPresent()){
            Group group = groupOptional.get();
            group.setName(newName);
            groupRepository.save(group);
        }
        else {
            throw new InvalidDataException("Department id not exist");
        }
    }

    public Group getGroup(long id){
        Optional<Group> groupOptional = groupRepository.findById(id);
        if(groupOptional.isPresent()){
            return groupOptional.get();
        }
        else return null;

    }
}
