package com.chnu.ejournal.dto;

public class LoginResponseDTO {

    private String email;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
