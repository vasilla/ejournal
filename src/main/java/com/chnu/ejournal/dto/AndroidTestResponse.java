package com.chnu.ejournal.dto;

public class AndroidTestResponse {
    private String test;

    public AndroidTestResponse(String test) {
        this.test = test;
    }

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }
}
