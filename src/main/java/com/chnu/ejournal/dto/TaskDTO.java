package com.chnu.ejournal.dto;

import com.chnu.ejournal.model.Course;
import com.chnu.ejournal.model.Task;
import com.chnu.ejournal.model.enums.TaskType;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

public class TaskDTO {
    private Long id;




    private TaskType type;


    private Double maxMark;

    private String name;

    public TaskDTO() {
    }

    public TaskDTO(Task e) {
        this.id = e.getId();
        this.maxMark = e.getMaxMark();
        this.name = e.getName();
        this.type = e.getType();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TaskType getType() {
        return type;
    }

    public void setType(TaskType type) {
        this.type = type;
    }

    public Double getMaxMark() {
        return maxMark;
    }

    public void setMaxMark(Double maxMark) {
        this.maxMark = maxMark;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TaskDTO)) return false;

        TaskDTO taskDTO = (TaskDTO) o;

        if (getId() != null ? !getId().equals(taskDTO.getId()) : taskDTO.getId() != null) return false;
        if (getType() != taskDTO.getType()) return false;
        if (getMaxMark() != null ? !getMaxMark().equals(taskDTO.getMaxMark()) : taskDTO.getMaxMark() != null)
            return false;
        return getName() != null ? getName().equals(taskDTO.getName()) : taskDTO.getName() == null;
    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getType() != null ? getType().hashCode() : 0);
        result = 31 * result + (getMaxMark() != null ? getMaxMark().hashCode() : 0);
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        return result;
    }
}
