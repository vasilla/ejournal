package com.chnu.ejournal.dto;

import com.chnu.ejournal.model.Student;

import javax.persistence.Column;

public class StudentDTO {

    private Long id;

    private String name;

    private String surname;

    private Integer subGroup;

    public StudentDTO() {
    }

    public StudentDTO(Student e) {
        this.id = e.getId();
        this.name = e.getName();
        this.surname = e.getSurname();
        this.subGroup = e.getSubGroup();
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Integer getSubGroup() {
        return subGroup;
    }

    public void setSubGroup(Integer subGroup) {
        this.subGroup = subGroup;
    }
}
