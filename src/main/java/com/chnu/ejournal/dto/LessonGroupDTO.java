package com.chnu.ejournal.dto;

import com.chnu.ejournal.model.LessonGroup;
import com.chnu.ejournal.model.enums.Days;
import com.chnu.ejournal.model.enums.LessonPlacement;
import com.chnu.ejournal.model.enums.LessonType;

import javax.persistence.Column;

public class LessonGroupDTO {

    private Long groupId;
    private Long lessonId;
    private Days day;
    private Integer lessonNumber;
    private LessonPlacement lessonPlacement;
    private String lessonName;
    private int subgroup;
    private Long image;

    public LessonGroupDTO() {
    }

    public LessonGroupDTO(LessonGroup e) {
        this.groupId = e.getGroup().getId();
        this.lessonId = e.getLesson().getId();
        this.day = e.getLesson().getDay();
        this.lessonNumber = e.getLesson().getLessonNumber();
        this.lessonPlacement = e.getLesson().getLessonPlacement();
        this.subgroup = e.getSubGroup();
        this.lessonName = e.getLesson().getCourse().getName();
        this.image = e.getLesson().getCourse().getImageId();
    }

    public Long getImage() {
        return image;
    }

    public void setImage(Long image) {
        this.image = image;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public Long getLessonId() {
        return lessonId;
    }

    public void setLessonId(Long lessonId) {
        this.lessonId = lessonId;
    }

    public Days getDay() {
        return day;
    }

    public void setDay(Days day) {
        this.day = day;
    }

    public Integer getLessonNumber() {
        return lessonNumber;
    }

    public void setLessonNumber(Integer lessonNumber) {
        this.lessonNumber = lessonNumber;
    }

    public LessonPlacement getLessonPlacement() {
        return lessonPlacement;
    }

    public void setLessonPlacement(LessonPlacement lessonPlacement) {
        this.lessonPlacement = lessonPlacement;
    }



    public String getLessonName() {
        return lessonName;
    }

    public void setLessonName(String lessonName) {
        this.lessonName = lessonName;
    }

    public int getSubgroup() {
        return subgroup;
    }

    public void setSubgroup(int subgroup) {
        this.subgroup = subgroup;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LessonGroupDTO)) return false;

        LessonGroupDTO that = (LessonGroupDTO) o;

        if (getSubgroup() != that.getSubgroup()) return false;
        if (getGroupId() != null ? !getGroupId().equals(that.getGroupId()) : that.getGroupId() != null) return false;
        if (getLessonId() != null ? !getLessonId().equals(that.getLessonId()) : that.getLessonId() != null)
            return false;
        if (getDay() != that.getDay()) return false;
        if (getLessonNumber() != null ? !getLessonNumber().equals(that.getLessonNumber()) : that.getLessonNumber() != null)
            return false;
        if (getLessonPlacement() != that.getLessonPlacement()) return false;
        return getLessonName() != null ? getLessonName().equals(that.getLessonName()) : that.getLessonName() == null;
    }

    @Override
    public int hashCode() {
        int result = getGroupId() != null ? getGroupId().hashCode() : 0;
        result = 31 * result + (getLessonId() != null ? getLessonId().hashCode() : 0);
        result = 31 * result + (getDay() != null ? getDay().hashCode() : 0);
        result = 31 * result + (getLessonNumber() != null ? getLessonNumber().hashCode() : 0);
        result = 31 * result + (getLessonPlacement() != null ? getLessonPlacement().hashCode() : 0);
        result = 31 * result + (getLessonName() != null ? getLessonName().hashCode() : 0);
        result = 31 * result + getSubgroup();
        return result;
    }
}
