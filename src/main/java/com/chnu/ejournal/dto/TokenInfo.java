package com.chnu.ejournal.dto;


import java.util.Map;

public class TokenInfo {
    private String iss;
    private String azp;
    private String aud;
    private String sub;
    private String email;
    private Boolean emailVerified;
    private String name;
    private String picture;
    private String givenName;
    private String familyName;
    private String locale;
    private String iat;
    private String exp;
    private String alg;
    private String kid;
    private String typ;


    public TokenInfo() {
    }

    public TokenInfo(Map<String,String> map) {
        this.iss = map.get("iss");
        this.azp = map.get("azp");
        this.aud = map.get("aud");
        this.sub = map.get("sub");
        this.email = map.get("email");
        this.emailVerified = Boolean.parseBoolean(map.get("email_verified"));//email_verified
        this.name = map.get("name");
        this.picture = map.get("picture");
        this.givenName = map.get("given_name");//given_name
        this.familyName = map.get("family_name");//family_name
        this.locale = map.get("locale");
        this.iat = map.get("iat");
        this.exp = map.get("exp");
        this.alg = map.get("alg");
        this.kid = map.get("kid");
        this.typ = map.get("typ");
    }

    public String getIss() {
        return iss;
    }

    public void setIss(String iss) {
        this.iss = iss;
    }

    public String getAzp() {
        return azp;
    }

    public void setAzp(String azp) {
        this.azp = azp;
    }

    public String getAud() {
        return aud;
    }

    public void setAud(String aud) {
        this.aud = aud;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(Boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getIat() {
        return iat;
    }

    public void setIat(String iat) {
        this.iat = iat;
    }

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

    public String getAlg() {
        return alg;
    }

    public void setAlg(String alg) {
        this.alg = alg;
    }

    public String getKid() {
        return kid;
    }

    public void setKid(String kid) {
        this.kid = kid;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    @Override
    public String toString() {
        return "TokenInfo{" +
                "iss='" + iss + '\'' +
                ", azp='" + azp + '\'' +
                ", aud='" + aud + '\'' +
                ", sub='" + sub + '\'' +
                ", email='" + email + '\'' +
                ", emailVerified=" + emailVerified +
                ", name='" + name + '\'' +
                ", picture='" + picture + '\'' +
                ", givenName='" + givenName + '\'' +
                ", familyName='" + familyName + '\'' +
                ", locale='" + locale + '\'' +
                ", iat='" + iat + '\'' +
                ", exp='" + exp + '\'' +
                ", alg='" + alg + '\'' +
                ", kid='" + kid + '\'' +
                ", typ='" + typ + '\'' +
                '}';
    }
}
