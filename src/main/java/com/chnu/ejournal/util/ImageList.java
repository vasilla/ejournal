package com.chnu.ejournal.util;

import java.util.ArrayList;
import java.util.List;

public class ImageList {
    private List<String> images;
    private final String PREFIX = "http://localhost:8080";

    public ImageList() {
        images = new ArrayList<>();
        images.add("/static/subjectImages/arch.jpg");
        images.add("/static/subjectImages/arch2.jpg");
        images.add("/static/subjectImages/arch3.jpg");
        images.add("/static/subjectImages/arch4.jpg");
        images.add("/static/subjectImages/arch5.jpg");
        images.add("/static/subjectImages/code.jpg");
        images.add("/static/subjectImages/code2.jpg");
        images.add("/static/subjectImages/code3.jpg");
        images.add("/static/subjectImages/crypto.jpg");
        images.add("/static/subjectImages/design.jpg");
        images.add("/static/subjectImages/design2.jpg");
        images.add("/static/subjectImages/math.jpg");
        images.add("/static/subjectImages/math2.jpg");
        images.add("/static/subjectImages/net.jpg");
        images.add("/static/subjectImages/net2.jpg");
        images.add("/static/subjectImages/qc.jpg");
        images.add("/static/subjectImages/qc2.jpg");
        images.add("/static/subjectImages/qc3.jpg");
        images.add("/static/subjectImages/subject0.jpg");
        images.add("/static/subjectImages/subject1.jpg");
        images.add("/static/subjectImages/subject2.jpg");
        images.add("/static/subjectImages/subject3.jpg");
        images.add("/static/subjectImages/subject4.jpg");
        images.add("/static/subjectImages/subject5.jpg");

//        setFullPath();//TODO fix costil
    }

    public void setFullPath(){
        for (int i = 0; i < images.size(); i++) {
            images.set(i,PREFIX+images.get(i));
        }
    }
    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public long getIndex(String image) {
        for (int i = 0; i < images.size(); i++) {
            if (image.contains(images.get(i))){
                return i;
            }
        }
        return 0;
    }
}
