package com.chnu.ejournal.exceptions;

public class UserRelatedException extends Exception {
    public UserRelatedException() {
        super();
    }

    public UserRelatedException(String message) {
        super(message);
    }

    public UserRelatedException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserRelatedException(Throwable cause) {
        super(cause);
    }

    protected UserRelatedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
