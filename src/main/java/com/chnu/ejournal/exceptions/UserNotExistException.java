package com.chnu.ejournal.exceptions;

public class UserNotExistException extends UserRelatedException {
    public UserNotExistException() {
    }

    public UserNotExistException(String message) {

        super(message);
    }

    public UserNotExistException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserNotExistException(Throwable cause) {
        super(cause);
    }

    public UserNotExistException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
