package com.chnu.ejournal.controller;

import com.chnu.ejournal.dao.*;
import com.chnu.ejournal.exceptions.InvalidDataException;
import com.chnu.ejournal.model.*;
import com.chnu.ejournal.util.ImageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.imageio.ImageIO;
import java.util.Map;
import java.util.Optional;

@Controller
public class ThymeleafPageController {

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private DepartmentsRepository departmentsRepository;

    @Autowired
    private ProfessorRepository professorRepository;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private CourseRepository courseRepository;

    private PageRequest createPageRequest(Optional<Integer> page,Optional<Integer> size){
        int currentPage = page.orElse(0);
        int pageSize = size.orElse(50);
        PageRequest pageRequest = PageRequest.of(currentPage, pageSize, Sort.Direction.ASC,"id" );
        return pageRequest;
    }

    private void setPaginationVariables(Map<String, Object> model,Page page){
//        model.put("items",page.getContent());
        model.put("currentPage",page.getPageable().getPageNumber());
        model.put("currentPageSize",page.getPageable().getPageSize());
        model.put("pageSize",page.getSize());
        if (page.hasNext()) {
            model.put("nextPage", page.nextPageable().getPageNumber());
            if (page.nextPageable().getPageNumber() < page.getTotalPages() -1){
                model.put("lastPage",page.getTotalPages() -1);
            }
        }
        if (page.hasPrevious()){
            model.put("previousPage", page.getNumber() -1);
//            model.put("firstPage",0);
            if (page.getNumber()>1){
                model.put("firstPage",0);
            }
        }
    }




    @GetMapping("/")
    public String getMainPage(){
        return "mainPage";
    }

    @GetMapping("/admins/edit/")
    public String  getEditAdminsPage(
            Map<String, Object> model,
            @RequestParam(name = "page",required = false) Optional<Integer> page,
            @RequestParam(name = "size",required = false) Optional<Integer> size
    ){

        Page<Admin> adminPage = adminRepository.findAll(createPageRequest(page,size));
        setPaginationVariables(model,adminPage);
        model.put("items",adminPage.getContent());
        return "editAdminsPage";
    }

    @GetMapping("/departments/edit/")
    public String  getEditDepartmentsPage(
            Map<String, Object> model,
            @RequestParam(name = "page",required = false) Optional<Integer> currentPage,
            @RequestParam(name = "size",required = false) Optional<Integer> size
    ){
        Page<Department> page = departmentsRepository.findAll(createPageRequest(currentPage,size));
        setPaginationVariables(model,page);
        model.put("items",page.getContent());
        return "editDepartmentsPage";
    }
    @GetMapping("/professors/edit/{departmentId}")
    public String  getEditProfessorsPage(
            @PathVariable(name = "departmentId") long departmentId,
            Map<String, Object> model,
            @RequestParam(name = "page",required = false) Optional<Integer> currentPage,
            @RequestParam(name = "size",required = false) Optional<Integer> size
    ) throws InvalidDataException {
        Optional<Department> superior = departmentsRepository.findById(departmentId);
        if (superior.isPresent()) {
            Page<Professor> page = professorRepository.findAllByDepartment(createPageRequest(currentPage, size),superior.get());
            setPaginationVariables(model, page);
            model.put("superior",superior.get());
            model.put("items", page.getContent());
            return "editProfessorsPage";
        }
        throw new InvalidDataException("departmentId invalid");
    }

    @GetMapping("/groups/edit/{departmentId}")
    public String  getEditGroupsPage(
            @PathVariable(name = "departmentId") long departmentId,
            Map<String, Object> model,
            @RequestParam(name = "page",required = false) Optional<Integer> currentPage,
            @RequestParam(name = "size",required = false) Optional<Integer> size
    ) throws InvalidDataException {
        Optional<Department> superior = departmentsRepository.findById(departmentId);
        if (superior.isPresent()) {
            Page<Group> page = groupRepository.findAllByDepartment(createPageRequest(currentPage, size),superior.get());
            setPaginationVariables(model, page);
            model.put("superior",superior.get());
            model.put("items", page.getContent());
            return "editGroupsPage";
        }
        throw new InvalidDataException("departmentId invalid");
    }

    @GetMapping("/group/edit/{groupId}")
    public String  getEditGroupPage(
            @PathVariable(name = "groupId") long groupId,
            Map<String, Object> model,
            @RequestParam(name = "page",required = false) Optional<Integer> currentPage,
            @RequestParam(name = "size",required = false) Optional<Integer> size
    ) throws InvalidDataException {
        Optional<Group> superior = groupRepository.findById(groupId);
        if (superior.isPresent()) {
            Page<Student> page = studentRepository.findAllByParentGroup(createPageRequest(currentPage, size),superior.get());
            setPaginationVariables(model, page);
            model.put("superior",superior.get());
            model.put("items", page.getContent());
            return "editGroupPage";
        }
        throw new InvalidDataException("groupId invalid");
    }

    @GetMapping("/courses/edit/{departmentId}")
    public String  getEditCoursesPage(
            @PathVariable(name = "departmentId") long departmentId,
            Map<String, Object> model,
            @RequestParam(name = "page",required = false) Optional<Integer> currentPage,
            @RequestParam(name = "size",required = false) Optional<Integer> size
    ) throws InvalidDataException {
        Optional<Department> superior = departmentsRepository.findById(departmentId);
        if (superior.isPresent()) {
            Page<Course> page = courseRepository.findAllByDepartment(createPageRequest(currentPage, size),superior.get());
            setPaginationVariables(model, page);
            model.put("superior",superior.get());
            model.put("items", page.getContent());
            model.put("images", new ImageList().getImages());
            return "editCoursePage";
        }
        throw new InvalidDataException("departmentId invalid");
    }


}
