package com.chnu.ejournal.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Map;

@Controller
public class ThymeliefTestController {

    @GetMapping("/page")
    public String testThymeleaf(Map<String, Object> model){
        model.put("message", "test message");
        return "thymeleaf";
    }
}
