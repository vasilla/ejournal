package com.chnu.ejournal.controller;

import com.chnu.ejournal.dto.LoginResponseDTO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
public class AuthorizationController {

    @GetMapping("/login")
    public String login(){
        return "loginPage";
    }





    @GetMapping("/profile")
    public String testMain(Authentication authentication, Map<String, Object> model){
//        model.put("email",authentication.getName());
        model.put("profile",authentication.toString());
        //System.out.println(authentication.toString());
        return "main";
    }


}
