package com.chnu.ejournal.controller.rest;

import com.chnu.ejournal.dao.AdminRepository;
import com.chnu.ejournal.dao.UserRepository;
import com.chnu.ejournal.exceptions.UserRelatedException;
import com.chnu.ejournal.service.CourseService;
import com.chnu.ejournal.service.DepartmentService;
import com.chnu.ejournal.service.GroupsService;
import com.chnu.ejournal.service.UserEditService;
import com.chnu.ejournal.util.ImageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Map;

@RestController
public class EditingController {


    @Autowired
    private UserEditService userEditService;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private GroupsService groupsService;

    @Autowired
    private CourseService courseService;



    @DeleteMapping("/admin/edit/")
    public ResponseEntity deleteAdmin(@RequestBody Map<String,String> map){
        try {
            userEditService.deleteAdmin(Long.parseLong(map.get("id")));
            return ResponseEntity.ok().body("done");
        }catch (UserRelatedException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    //TODO add backend validation
    @PatchMapping("/admin/edit/")
    public ResponseEntity editEmailOfAdmin(@RequestBody Map<String,String> map){
        try {
            userEditService.editEmailOfAdmin(Long.parseLong(map.get("id")),map.get("email"));
            return ResponseEntity.ok().body("done");
        }catch (UserRelatedException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    //TODO add backend validation
    @PostMapping("/admin/edit/")
    public ResponseEntity addNewAdmin(@RequestBody Map<String,String> map){
        try {
            userEditService.createNewAdmin(map.get("email"));
            return ResponseEntity.ok().body("done");
        }catch (UserRelatedException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @DeleteMapping("/department/edit/")
    public ResponseEntity deleteDepartment(@RequestBody Map<String,String> map){
        try {
            departmentService.deleteDepartment(Long.parseLong(map.get("id")));
            return ResponseEntity.ok().body("done");
        }catch (Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    //TODO add backend validation
    @PatchMapping("/department/edit/")
    public ResponseEntity editDepartment(@RequestBody Map<String,String> map){
        try {
            departmentService.updateDepartment(Long.parseLong(map.get("id")),map.get("name"));
            return ResponseEntity.ok().body("done");
        }catch (Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    //TODO add backend validation
    @PostMapping("/department/edit/")
    public ResponseEntity addDepartment(@RequestBody Map<String,String> map){
        try {
            departmentService.creaateDepartment(map.get("name"));
            return ResponseEntity.ok().body("done");
        }catch (Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @DeleteMapping("/professors/edit/")
    public ResponseEntity deleteProfessor(@RequestBody Map<String,String> map){
        try {
            userEditService.deleteProfessor(Long.parseLong(map.get("id")));
            return ResponseEntity.ok().body("done");
        }catch (Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    //TODO add backend validation
    @PatchMapping("/professors/edit/")
    public ResponseEntity editProfessor(@RequestBody Map<String,String> map){
        try {
            userEditService.editProfessor(Long.parseLong(map.get("id")),map.get("email"),
                    Boolean.parseBoolean(map.get("admin")));
            return ResponseEntity.ok().body("done");
        }catch (Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    //TODO add backend validation
    @PostMapping("/professors/edit/")
    public ResponseEntity addProfessor(@RequestBody Map<String,String> map){
        try {
            userEditService.createProfessor(map.get("email"),
                    Boolean.parseBoolean(map.get("admin")),Long.parseLong(map.get("departmentId")));
            return ResponseEntity.ok().body("done");
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @DeleteMapping("/groups/edit/")
    public ResponseEntity deleteGroup(@RequestBody Map<String,String> map){
        try {
            groupsService.deleteGroup(Long.parseLong(map.get("id")));
            return ResponseEntity.ok().body("done");
        }catch (Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    //TODO add backend validation
    @PatchMapping("/groups/edit/")
    public ResponseEntity editGroup(@RequestBody Map<String,String> map){
        try {
            groupsService.editGroup(Long.parseLong(map.get("id")),map.get("name"));
            return ResponseEntity.ok().body("done");
        }catch (Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    //TODO add backend validation
    @PostMapping("/groups/edit/")
    public ResponseEntity addGroup(@RequestBody Map<String,String> map){
        try {
            groupsService.createGroup(map.get("name"),Long.parseLong(map.get("departmentId")));
            return ResponseEntity.ok().body("done");
        }catch (Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @DeleteMapping("/group/edit/")
    public ResponseEntity deleteStudent(@RequestBody Map<String,String> map){
        try {
            userEditService.deleteStudent(Long.parseLong(map.get("id")));
            return ResponseEntity.ok().body("done");
        }catch (Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    //TODO add backend validation
    @PatchMapping("/group/edit/")
    public ResponseEntity editStudent(@RequestBody Map<String,String> map){
        try {
            userEditService.editStudent(Long.parseLong(map.get("id")),map.get("email"),
                    Byte.parseByte(map.get("subgroup")));
            return ResponseEntity.ok().body("done");
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    //TODO add backend validation
    @PostMapping("/group/edit/")
    public ResponseEntity addStudent(@RequestBody Map<String,String> map){
        try {
            userEditService.createStudent(map.get("email"),
                    Long.parseLong(map.get("groupId")),Byte.parseByte(map.get("subgroup")));
            return ResponseEntity.ok().body("done");
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @DeleteMapping("/course/edit/")
    public ResponseEntity deleteCourse(@RequestBody Map<String,String> map){
        try {
            courseService.deleteCourse(Long.parseLong(map.get("id")));
            return ResponseEntity.ok().body("done");
        }catch (Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    //TODO add backend validation
    @PatchMapping("/course/edit/")
    public ResponseEntity editCourse(@RequestBody Map<String,String> map){
        try {
            courseService.editCourse(Long.parseLong(map.get("id")),map.get("name"),
                    new ImageList().getIndex(map.get("image")));
            return ResponseEntity.ok().body("done");
        }catch (Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    //TODO add backend validation
    @PostMapping("/course/edit/")
    public ResponseEntity addCourse(@RequestBody Map<String,String> map){
        try {
            courseService.createCourse(map.get("name"),
                    new ImageList().getIndex(map.get("image")),
                    Long.parseLong(map.get("departmentId")));
            return ResponseEntity.ok().body("done");
        }catch (Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }


}
