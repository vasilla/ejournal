package com.chnu.ejournal.controller.rest;

import com.chnu.ejournal.dao.*;
import com.chnu.ejournal.dto.GroupDTO;
import com.chnu.ejournal.dto.LessonGroupDTO;
import com.chnu.ejournal.dto.StudentDTO;
import com.chnu.ejournal.dto.TaskDTO;
import com.chnu.ejournal.exceptions.InvalidDataException;
import com.chnu.ejournal.model.*;
import com.chnu.ejournal.model.enums.Days;
import com.chnu.ejournal.model.enums.LessonPlacement;
import com.chnu.ejournal.model.enums.LessonType;
import com.chnu.ejournal.model.enums.TaskType;
import com.chnu.ejournal.service.CourseService;
import com.chnu.ejournal.service.GroupsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.Table;
import java.util.*;

@RestController
public class AndroidRestController {



    @Autowired
    private GroupsService groupsService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private ProfessorRepository professorRepository;

    @Autowired
    private LessonRepository lessonRepository;

    @Autowired
    private LessonGroupRepository lessonGroupRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private MarkRepository markRepository;



    private int numberOfWeek = 1;

    @GetMapping("/numberofweek/")
    public Integer getNumberOfWeek(){
        return numberOfWeek;
    }

    @GetMapping("/numberofweek/change")
    public Integer changeNumberOfWeek(){
        if (numberOfWeek == 1){
            numberOfWeek = 2;
        }
        else {
            numberOfWeek = 1;
        }
        return numberOfWeek;
    }

    @GetMapping("/group/students/{id}")
    public List<StudentDTO> getGroupStudents(@PathVariable(name = "id") long id){
        Optional<Group> superior = groupRepository.findById(id);
        if (superior.isPresent()) {
            Iterable<Student> studentIterable = studentRepository.findAllByParentGroup(superior.get());
            List<StudentDTO> list = new ArrayList<>();
            studentIterable.forEach(e->list.add(new StudentDTO(e)));
            return list;
        }
        else return null;
    }

    @GetMapping("/group/{groupId}")
    public GroupDTO getGroup(@PathVariable(name = "groupId") long groupId){
        Optional<Group> superior = groupRepository.findById(groupId);
        if (superior.isPresent()) {

            GroupDTO groupDTO = new GroupDTO();
            groupDTO.setName(superior.get().getName());
            groupDTO.setId(superior.get().getId());
            return groupDTO;
        }
        else return null;
    }

    @GetMapping("/lessons/student/{studentId}")
    public Set<LessonGroupDTO> getLessonsByGroup(@PathVariable(name = "studentId") long studentId){
        Optional<Student> superior = studentRepository.findById(studentId);
        if (superior.isPresent()) {
            Student student = superior.get();
            Set<LessonGroupDTO> list = new HashSet<>();
            Iterable<LessonGroup> allByGroupAndSubGroup1 = lessonGroupRepository.findAllByGroupAndSubGroup(student.getParentGroup(), student.getSubGroup());
            Iterable<LessonGroup> allByGroupAndSubGroup2 = lessonGroupRepository.findAllByGroupAndSubGroup(student.getParentGroup(), 0);
            allByGroupAndSubGroup1.forEach(e->list.add(new LessonGroupDTO(e)));
            allByGroupAndSubGroup2.forEach(e->list.add(new LessonGroupDTO(e)));
            return list;
        }
        else return null;
    }

    @GetMapping("/lessons/professor/{professorId}")
    public Set<LessonGroupDTO> getLessonsByProfessor(@PathVariable(name = "professorId") long professorId){
        Optional<Professor> superior = professorRepository.findById(professorId);
        if (superior.isPresent()) {
            Set<LessonGroupDTO> resultSet = new HashSet<>();
            Iterable<Lesson> iterable = lessonRepository.findAllByProfessor(superior.get());
            iterable.forEach(e->{
                lessonGroupRepository.findAllByLesson(e).forEach(e2->resultSet.add(new LessonGroupDTO(e2)));
            });
            return resultSet;
        }
        else return null;
    }

    @GetMapping("lesson/tasks/{lessonId}")
    public Set<TaskDTO> getTasksByLesson(@PathVariable(name = "lessonId") long lessonId){
        Optional<Lesson> superior = lessonRepository.findById(lessonId);
        if (superior.isPresent()) {
            Set<TaskDTO> resultSet = new HashSet<>();
            Iterable<Task> iterable = taskRepository.findAllByCourse(superior.get().getCourse());
            iterable.forEach(e->{
                resultSet.add(new TaskDTO(e));
            });
            return resultSet;
        }
        else return null;
    }

    @GetMapping("student/{studentId}/task/{taskId}")
    public double getMarkByStudentAndTask(@PathVariable(name = "studentId") long studentId,
                                          @PathVariable(name = "taskId") long taskId){
        Optional<Student> studentOptional = studentRepository.findById(studentId);
        Optional<Task> taskOptional = taskRepository.findById(taskId);
        if (studentOptional.isPresent()&&taskOptional.isPresent()) {
            Optional<Mark> markOptional = markRepository.findById(new MarkPrimaryKey(taskOptional.get(), studentOptional.get()));
            if (markOptional.isPresent()){
                return markOptional.get().getMark();
            }
            else return 0;
        }
        return 0;
    }

    @PostMapping("student/{studentId}/task/{taskId}/points/{points}")
    public void setMarkByStudentAndTask(@PathVariable(name = "studentId") long studentId,
                                          @PathVariable(name = "taskId") long taskId,
                                          @PathVariable(name = "points") double points
                                          ){
        Optional<Student> studentOptional = studentRepository.findById(studentId);
        Optional<Task> taskOptional = taskRepository.findById(taskId);
        if (studentOptional.isPresent()&&taskOptional.isPresent()) {
            MarkPrimaryKey markPrimaryKey = new MarkPrimaryKey(taskOptional.get(), studentOptional.get());
            Mark mark = markRepository.findById(markPrimaryKey).orElse(new Mark(markPrimaryKey));
            if (points>taskOptional.get().getMaxMark()){
                mark.setMark(taskOptional.get().getMaxMark());
            }
            if (points<0){
                return;
            }
            mark.setMark(points);
            markRepository.save(mark);
        }
    }



    @GetMapping("/setTestData")
    public String setTestData(){
        try {
//            courseService.createCourse("Вимоги до ПЗ",13L,1L);
//            courseService.createCourse("Криптографія",8L,1L);
//            courseService.createCourse("Архітектура комп'ютера",9L,1L);

            /*
            Course course1 = courseRepository.findById(1L).orElse(null);
            Course course2 = courseRepository.findById(1L).orElse(null);
            Course course3 = courseRepository.findById(1L).orElse(null);
            Group group = groupRepository.findAll().iterator().next();
            Professor professor3 = professorRepository.findById(1L).orElse(null);
            Professor professor2 = professorRepository.findById(2L).orElse(null);
            Professor professor1 = professorRepository.findById(3L).orElse(null);

            Lesson lesson = new Lesson();
            lesson.setDay(Days.MON);
            lesson.setCourse(course1);
            lesson.setAuditory("8-202");
            lesson.setProfessor(professor1);
            lesson.setLessonNumber((byte) 3);
            lesson.setLessonPlacement(LessonPlacement.EVERY_WEEK);
            lessonRepository.save(lesson);

            LessonGroup lessonGroup = new LessonGroup();
            lessonGroup.setGroup(group);
            lessonGroup.setLesson(lesson);
            lessonGroup.setSubGroup((byte)0);
            lessonGroupRepository.save(lessonGroup);

            lesson = new Lesson();
            lesson.setDay(Days.THU);
            lesson.setCourse(course1);
            lesson.setAuditory("8-202");
            lesson.setProfessor(professor1);
            lesson.setLessonNumber((byte) 3);
            lesson.setLessonPlacement(LessonPlacement.EVERY_WEEK);
            lessonRepository.save(lesson);

            lessonGroup = new LessonGroup();
            lessonGroup.setGroup(group);
            lessonGroup.setLesson(lesson);
            lessonGroup.setSubGroup((byte)0);
            lessonGroupRepository.save(lessonGroup);

            lesson = new Lesson();
            lesson.setDay(Days.WED);
            lesson.setCourse(course2);
            lesson.setAuditory("8-202");
            lesson.setProfessor(professor1);
            lesson.setLessonNumber((byte) 3);
            lesson.setLessonPlacement(LessonPlacement.EVERY_WEEK);
            lessonRepository.save(lesson);

            lessonGroup = new LessonGroup();
            lessonGroup.setGroup(group);
            lessonGroup.setLesson(lesson);
            lessonGroup.setSubGroup((byte)0);
            lessonGroupRepository.save(lessonGroup);

            lesson = new Lesson();
            lesson.setDay(Days.WED);
            lesson.setCourse(course3);
            lesson.setAuditory("8-202");
            lesson.setProfessor(professor3);
            lesson.setLessonNumber((byte) 4);
            lesson.setLessonPlacement(LessonPlacement.EVERY_WEEK);
            lesson.setLessonType(LessonType.LECTURE);
            lessonRepository.save(lesson);

            lessonGroup = new LessonGroup();
            lessonGroup.setGroup(group);
            lessonGroup.setLesson(lesson);
            lessonGroup.setSubGroup((byte)0);
            lessonGroupRepository.save(lessonGroup);

            */
            /*
            lessonRepository.findAll().forEach(e->{
                e.setLessonType(LessonType.LECTURE);
                lessonRepository.save(e);
            });
            */

            courseRepository.findAll().forEach(e->{
                int labNumber = 4 + new Random().nextInt(4);
                Task task;
                for (int i = 0; i < labNumber ; i++) {
                    task = new Task();
                    task.setCourse(e);
                    task.setType(TaskType.LAB);
                    if (i%4==0) {
                        task.setType(TaskType.CR);
                    }
                    task.setMaxMark(Math.floor(100.0/labNumber));
                    task.setName("task#"+String.valueOf(i+1));
                    taskRepository.save(task);
                }
            });
        throw new InvalidDataException();


        } catch (InvalidDataException e) {
            e.printStackTrace();
        }
        return "done";
    }
}
