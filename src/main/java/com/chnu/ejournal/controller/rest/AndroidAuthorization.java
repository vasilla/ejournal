package com.chnu.ejournal.controller.rest;

import com.chnu.ejournal.dto.AndroidTestResponse;
import com.chnu.ejournal.dto.LoginResponseDTO;
import com.chnu.ejournal.exceptions.TokenInvalidException;
import com.chnu.ejournal.exceptions.UserNotExistException;
import com.chnu.ejournal.model.enums.Role;
import com.chnu.ejournal.model.Student;
import com.chnu.ejournal.model.User;
import com.chnu.ejournal.model.UserPrincipal;
import com.chnu.ejournal.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.lang.Nullable;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.view.RedirectView;

import java.util.Arrays;
import java.util.Map;

@RestController
public class AndroidAuthorization {

    @Autowired
    private AuthService authService;


    @PostMapping("/auth")
    public User androidLogin(@Nullable @RequestBody String body) {
        LoginResponseDTO loginResponseDTO = new LoginResponseDTO();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String token = body.substring(1,body.length()-1);
        Map map = restTemplate.getForObject(authService.getGoogleTokenInfoLink(token), Map.class);
        try {
            return authService.processToken(map);
        } catch (UserNotExistException | TokenInvalidException e) {
            return null;
        }

    }



//    @PostMapping("/just_get")
//    public AndroidTestResponse getUnsecured2() {
//        System.out.println("getUnsecured invoked!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
//        return new AndroidTestResponse("post without protection response");
//    }
//    @GetMapping("/just_get")
//    public AndroidTestResponse getUnsecured() {
//        System.out.println("getUnsecured invoked!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
//        SecurityContext securityContext = SecurityContextHolder.getContext();
//        User user =new Student();
//        user.setEmail("chnumarks@gmail.com");
//
//        Authentication authentication = new UsernamePasswordAuthenticationToken(new UserPrincipal(user),
//                null, Arrays.asList(new SimpleGrantedAuthority(Role.STUDENT.toString())));
//
//        System.out.println("isAuthenticated == "+authentication.isAuthenticated());
//
////        securityContext.setAuthentication(authentication);
//
////        UsernamePasswordAuthenticationToken authReq
////                = new UsernamePasswordAuthenticationToken(, null,BCrypt.gensalt()));
////        Authentication authentication = authenticationManager.authenticate(authReq);
////        SecurityContext securityContext = SecurityContextHolder.getContext();
//        securityContext.setAuthentication(authentication);
//        return new AndroidTestResponse("get without protection response");
//    }
//
//    @GetMapping("/secure_get")
//    public AndroidTestResponse getSecured(Authentication authentication) {
//        System.out.println("getSecured invoked!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
//
//        return new AndroidTestResponse("get with protection response authentication exists: " + (authentication != null));
//    }

    @RequestMapping(value = "/auth/success")
    public RedirectView userInfo(OAuth2AuthenticationToken token) {
        if (authService.authenticate(token)) {
            return new RedirectView("/");
        }
        else {
            return new RedirectView("/login?error");
        }
    }

}