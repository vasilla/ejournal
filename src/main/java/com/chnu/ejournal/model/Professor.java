package com.chnu.ejournal.model;

import javax.persistence.*;

@Entity
public class Professor extends User{

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "department_id")
    private Department department;



    @Column(name = "is_admin")
    private Boolean isAdmin;


    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Boolean getAdmin() {
        return isAdmin;
    }

    public void setAdmin(Boolean admin) {
        isAdmin = admin;
    }

    @Override
    public String toString() {
        return "Professor{" +
                "isAdmin=" + isAdmin +
                "} " + super.toString();
    }
}
