package com.chnu.ejournal.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "delayed_lessons")
public class DelayedLesson {
    @Id
    @SequenceGenerator(name = "course_sequence", sequenceName = "course_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "course_sequence")
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "lesson_id")
    private Lesson lesson;

    @Column(name = "date_from")
    private LocalDate localDate;

    @Column(name = "date_to")
    private LocalDate to;

    @Column(name = "lesson_number")
    private Byte lessonNumber;

    @Column(name = "auditory")
    private String auditory;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Lesson getLesson() {
        return lesson;
    }

    public void setLesson(Lesson lesson) {
        this.lesson = lesson;
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public void setLocalDate(LocalDate localDate) {
        this.localDate = localDate;
    }

    public LocalDate getTo() {
        return to;
    }

    public void setTo(LocalDate to) {
        this.to = to;
    }

    public Byte getLessonNumber() {
        return lessonNumber;
    }

    public void setLessonNumber(Byte lessonNumber) {
        this.lessonNumber = lessonNumber;
    }

    public String getAuditory() {
        return auditory;
    }

    public void setAuditory(String auditory) {
        this.auditory = auditory;
    }
}
