package com.chnu.ejournal.model;

import javax.persistence.*;

@Entity
public class Student extends User {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "group_id")
    private Group parentGroup;

    @Column(name = "sub_group")
    private Integer subGroup;

    public Student() {
    }

    public Student(String name, String surname, String email, Long id) {
        super(name, surname, email, id);
    }


    public Group getParentGroup() {
        return parentGroup;
    }

    public void setParentGroup(Group parentGroup) {
        this.parentGroup = parentGroup;
    }

    public Integer getSubGroup() {
        return subGroup;
    }

    public void setSubGroup(Integer subGroup) {
        this.subGroup = subGroup;
    }

    public String getSubgroupStr(){
        return String.valueOf(subGroup);
    }
}
