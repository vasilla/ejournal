package com.chnu.ejournal.model;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class MarkPrimaryKey implements Serializable{

    private static final long serialVersionUID = 1L;


    @ManyToOne()
    @JoinColumn(name = "task_id")
    private Task task;

    @ManyToOne()
    @JoinColumn(name = "student_id")
    private Student student;

    public MarkPrimaryKey() {
    }

    public MarkPrimaryKey(Task task, Student student) {
        this.task = task;
        this.student = student;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}
