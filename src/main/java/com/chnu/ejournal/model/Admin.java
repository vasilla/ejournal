package com.chnu.ejournal.model;

import javax.persistence.Entity;

@Entity
public class Admin extends User {

    @Override
    public String toString() {
        return "Admin{} " + super.toString();
    }
}
