package com.chnu.ejournal.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "groups")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Group {
    @Id
    @SequenceGenerator(name = "group_sequence", sequenceName = "group_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "group_sequence")
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "department_id")
    private Department department;

    @Column(name = "name")
    private String name;

    @Column(name = "subgroups_counter")
    private Integer subgroupsCounter;




//    @LazyCollection(LazyCollectionOption.TRUE)
//    @OneToMany
//    @JoinColumn(name = "group_id")
//    private Set<Student> students;

//    @LazyCollection(LazyCollectionOption.FALSE)
//    @OneToMany(fetch = FetchType.EAGER)
//    @JoinColumn(name = "group_id")
//    private Set<LessonGroup> groups;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public Set<LessonGroup> getGroups() {
//        return groups;
//    }
//
//    public void setGroups(Set<LessonGroup> groups) {
//        this.groups = groups;
//    }

    public Integer getSubgroupsCounter() {
        return subgroupsCounter;
    }

    public void setSubgroupsCounter(Integer subgroupsCounter) {
        this.subgroupsCounter = subgroupsCounter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Group)) return false;

        Group group = (Group) o;

        if (getId() != null ? !getId().equals(group.getId()) : group.getId() != null) return false;
        if (getName() != null ? !getName().equals(group.getName()) : group.getName() != null) return false;
        return getSubgroupsCounter() != null ? getSubgroupsCounter().equals(group.getSubgroupsCounter()) : group.getSubgroupsCounter() == null;
    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + (getSubgroupsCounter() != null ? getSubgroupsCounter().hashCode() : 0);
        return result;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }
}
