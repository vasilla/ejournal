package com.chnu.ejournal.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "marks")
public class Mark {

    @EmbeddedId
    private MarkPrimaryKey markPrimaryKey;

    @Column(name = "mark")
    private Double mark;

    public Mark() {
    }

    public Mark(MarkPrimaryKey markPrimaryKey, Double mark) {
        this.markPrimaryKey = markPrimaryKey;
        this.mark = mark;
    }

    public Mark(MarkPrimaryKey markPrimaryKey) {
        this.markPrimaryKey = markPrimaryKey;
    }

    public MarkPrimaryKey getMarkPrimaryKey() {
        return markPrimaryKey;
    }

    public void setMarkPrimaryKey(MarkPrimaryKey markPrimaryKey) {
        this.markPrimaryKey = markPrimaryKey;
    }

    public Double getMark() {
        return mark;
    }

    public void setMark(Double mark) {
        this.mark = mark;
    }
}
