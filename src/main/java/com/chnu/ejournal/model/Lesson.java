package com.chnu.ejournal.model;

import com.chnu.ejournal.model.enums.Days;
import com.chnu.ejournal.model.enums.LessonPlacement;
import com.chnu.ejournal.model.enums.LessonType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "lessons")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Lesson {
    @Id
    @SequenceGenerator(name = "lesson_sequence", sequenceName = "lesson_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "lesson_sequence")
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "course_id")
    private Course course;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "professor_id")
    private Professor professor;

//    @LazyCollection(LazyCollectionOption.TRUE)
//    @OneToMany
//    @JoinColumn(name = "lesson_id")
//    private Set<LessonGroup> groups;



    @Column(name = "type")
    private LessonType lessonType;

    @Column(name = "day")
    private Days day;

    @Column(name = "lesson_number")
    private Integer lessonNumber;

    @Column(name = "auditory")
    private String auditory;

    @Column(name = "placement")
    private LessonPlacement lessonPlacement;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    public LessonType getLessonType() {
        return lessonType;
    }

    public void setLessonType(LessonType lessonType) {
        this.lessonType = lessonType;
    }

    public Days getDay() {
        return day;
    }

    public void setDay(Days day) {
        this.day = day;
    }

    public Integer getLessonNumber() {
        return lessonNumber;
    }

    public void setLessonNumber(Integer lessonNumber) {
        this.lessonNumber = lessonNumber;
    }

    public String getAuditory() {
        return auditory;
    }

    public void setAuditory(String auditory) {
        this.auditory = auditory;
    }

    public LessonPlacement getLessonPlacement() {
        return lessonPlacement;
    }

    public void setLessonPlacement(LessonPlacement lessonPlacement) {
        this.lessonPlacement = lessonPlacement;
    }

//    public Set<LessonGroup> getGroups() {
//        return groups;
//    }
//
//    public void setGroups(Set<LessonGroup> groups) {
//        this.groups = groups;
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Lesson)) return false;

        Lesson lesson = (Lesson) o;

        if (getId() != null ? !getId().equals(lesson.getId()) : lesson.getId() != null) return false;
        if (getCourse() != null ? !getCourse().equals(lesson.getCourse()) : lesson.getCourse() != null) return false;
        if (getProfessor() != null ? !getProfessor().equals(lesson.getProfessor()) : lesson.getProfessor() != null)
            return false;
        if (getLessonType() != lesson.getLessonType()) return false;
        if (getDay() != lesson.getDay()) return false;
        if (getLessonNumber() != null ? !getLessonNumber().equals(lesson.getLessonNumber()) : lesson.getLessonNumber() != null)
            return false;
        if (getAuditory() != null ? !getAuditory().equals(lesson.getAuditory()) : lesson.getAuditory() != null)
            return false;
        return getLessonPlacement() == lesson.getLessonPlacement();
    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getCourse() != null ? getCourse().hashCode() : 0);
        result = 31 * result + (getProfessor() != null ? getProfessor().hashCode() : 0);
        result = 31 * result + (getLessonType() != null ? getLessonType().hashCode() : 0);
        result = 31 * result + (getDay() != null ? getDay().hashCode() : 0);
        result = 31 * result + (getLessonNumber() != null ? getLessonNumber().hashCode() : 0);
        result = 31 * result + (getAuditory() != null ? getAuditory().hashCode() : 0);
        result = 31 * result + (getLessonPlacement() != null ? getLessonPlacement().hashCode() : 0);
        return result;
    }
}
