package com.chnu.ejournal.model;

import com.chnu.ejournal.model.enums.Role;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCrypt;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class UserPrincipal implements UserDetails {

    private User user;

    private static final String ROLE_PREFIX = "ROLE_";

    public UserPrincipal(User user) {
        this.user = user;
    }

    private List<Role> getUserRole(User user){
        List<Role> roles = new ArrayList<>();
        if (user instanceof Admin) {
            roles.add(Role.ADMIN);
            return roles;
        }


        if (user instanceof Professor) {
            roles.add(Role.PROFESSOR);
            if (((Professor)user).getAdmin()){
                roles.add(Role.ADMIN);
            }
            return roles;

        }
        roles.add(Role.STUDENT);
        return roles;
    }

    public static  boolean isAdmin(User user){
        return (user instanceof Admin)||
                ((user instanceof Professor)&&(((Professor)user).getAdmin()));

    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List list = new ArrayList<SimpleGrantedAuthority>();
        List<Role> roles = getUserRole(user);
        for(Role role : roles) {
            list.add(new SimpleGrantedAuthority(ROLE_PREFIX + role.toString()));
        }
        return list;
    }

    @Override
    public String getPassword() {
        return BCrypt.hashpw(user.getEmail(),BCrypt.gensalt());
    }

    @Override
    public String getUsername() {
        return user.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
