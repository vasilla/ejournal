package com.chnu.ejournal.model.enums;

public enum Role {
    STUDENT,
    PROFESSOR,
    ADMIN
}
