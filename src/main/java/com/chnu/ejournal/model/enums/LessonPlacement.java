package com.chnu.ejournal.model.enums;

public enum LessonPlacement {
    EVERY_FIRST_WEEK,EVERY_SECONDARY_WEEK,EVERY_WEEK
}
