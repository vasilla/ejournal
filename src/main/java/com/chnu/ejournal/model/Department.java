package com.chnu.ejournal.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "department")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Department {

    @Id
    @SequenceGenerator(name = "department_sequence", sequenceName = "department_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "department_sequence")
    @Column(name = "id")
    private Long id;


    @Column(name = "name")
    private String name;

//    @LazyCollection(LazyCollectionOption.TRUE)
//    @OneToMany
//    @JoinColumn(name = "department_id")
//    private Set<Group> groups;


//    @OneToMany(fetch = FetchType.LAZY)
//    @JoinColumn(name = "department_id")
//    private Set<Course> courses;
//
//    @LazyCollection(LazyCollectionOption.TRUE)
//    @OneToMany()
//    @JoinColumn(name = "department_id")
//    private Set<Professor> professors;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public Set<Group> getGroups() {
//        return groups;
//    }
//
//    public void setGroups(Set<Group> groups) {
//        this.groups = groups;
//    }

//    public Set<Professor> getProfessors() {
//        return professors;
//    }
//
//    public void setProfessors(Set<Professor> professors) {
//        this.professors = professors;
//    }
}
