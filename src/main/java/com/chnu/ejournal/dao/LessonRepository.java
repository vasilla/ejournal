package com.chnu.ejournal.dao;

import com.chnu.ejournal.model.Admin;
import com.chnu.ejournal.model.Lesson;
import com.chnu.ejournal.model.Professor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LessonRepository extends PagingAndSortingRepository<Lesson,Long> {

    Iterable<Lesson> findAllByProfessor(Professor professor);
}
