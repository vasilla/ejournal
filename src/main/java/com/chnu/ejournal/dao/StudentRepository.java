package com.chnu.ejournal.dao;

import com.chnu.ejournal.model.Admin;
import com.chnu.ejournal.model.Group;
import com.chnu.ejournal.model.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends PagingAndSortingRepository<Student,Long>{
    Page<Student> findAllByParentGroup(Pageable pageable, Group group);
    Iterable<Student> findAllByParentGroup(Group group);
}
