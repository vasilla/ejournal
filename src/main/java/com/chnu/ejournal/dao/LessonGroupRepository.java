package com.chnu.ejournal.dao;

import com.chnu.ejournal.model.Admin;
import com.chnu.ejournal.model.Group;
import com.chnu.ejournal.model.Lesson;
import com.chnu.ejournal.model.LessonGroup;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LessonGroupRepository extends PagingAndSortingRepository<LessonGroup,Long> {
    Iterable<LessonGroup> findAllByGroupAndSubGroup(Group group,Integer subgroup);
    Iterable<LessonGroup> findAllByLesson(Lesson lesson);

}
