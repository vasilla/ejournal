package com.chnu.ejournal.dao;

import com.chnu.ejournal.model.Course;
import com.chnu.ejournal.model.Student;
import com.chnu.ejournal.model.Task;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskRepository extends PagingAndSortingRepository<Task,Long> {

    Iterable<Task> findAllByCourse(Course course);
}
