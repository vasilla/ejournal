package com.chnu.ejournal.dao;

import com.chnu.ejournal.model.Admin;
import com.chnu.ejournal.model.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminRepository extends PagingAndSortingRepository<Admin,Long> {
    Admin getAdminByEmail(String email);
    
}
