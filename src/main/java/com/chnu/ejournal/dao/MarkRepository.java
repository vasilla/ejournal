package com.chnu.ejournal.dao;

import com.chnu.ejournal.model.Mark;
import com.chnu.ejournal.model.MarkPrimaryKey;
import com.chnu.ejournal.model.Task;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MarkRepository extends PagingAndSortingRepository<Mark,MarkPrimaryKey> {

//    Optional<Mark> f(Long aLong);
}
