package com.chnu.ejournal.dao;

import com.chnu.ejournal.model.Admin;
import com.chnu.ejournal.model.Course;
import com.chnu.ejournal.model.Department;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends PagingAndSortingRepository<Course,Long> {
    Page<Course> findAllByDepartment(Pageable pageable, Department department);
}
