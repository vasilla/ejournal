package com.chnu.ejournal.dao;

import com.chnu.ejournal.model.Admin;
import com.chnu.ejournal.model.Department;
import com.chnu.ejournal.model.Professor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfessorRepository extends PagingAndSortingRepository<Professor,Long> {
    Page<Professor> findAllByDepartment(Pageable pageable, Department department);
}
