package com.chnu.ejournal.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@Configuration
@PropertySources({
        @PropertySource("classpath:application.properties"),
        @PropertySource("classpath:social.properties")
})

public class PropertiesConfig {

    @Value("${spring.security.oauth2.client.registration.google.clientId}")
    private String googleClientIdForServer;

    @Value("${androidClientId1}")
    private String googleClientIdForAndroid1;

    @Value("${androidClientId2}")
    private String googleClientIdForAndroid2;

    @Value("${tokenInfo}")
    private String googleTokenInfoLink;

    @Value("${defaultAdmin}")
    private String defaultAdminEmail;


    @Bean(name = "googleClientIdForServer")
    public String getGoogleClientIdForServer() {
        return googleClientIdForServer;
    }

    @Bean(name = "googleClientIdForAndroid1")
    public String getGoogleClientIdForAndroid1() {
        return googleClientIdForAndroid1;
    }

    @Bean(name = "googleClientIdForAndroid2")
    public String getGoogleClientIdForAndroid2() {
        return googleClientIdForAndroid2;
    }

    @Bean(name = "googleTokenInfoLink")
    public String getGoogleTokenInfoLink() {
        return googleTokenInfoLink;
    }

    @Bean(name = "defaultAdminEmail")
    public String getDefaultAdminEmail() {
        return defaultAdminEmail;
    }
}
