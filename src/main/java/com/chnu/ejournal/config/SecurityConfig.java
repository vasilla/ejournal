package com.chnu.ejournal.config;

import com.chnu.ejournal.service.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
@EnableOAuth2Client
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf()
                .disable()
                .antMatcher("/**")
                .authorizeRequests()
                .antMatchers("/static/**").permitAll()
                .antMatchers("/just_get").permitAll()
                .antMatchers("/login").permitAll()
                .antMatchers("/").authenticated()
                .antMatchers("/secure_get").permitAll()
                .antMatchers("/profile").authenticated()
                .antMatchers("/loginPage").permitAll()
                .anyRequest().permitAll()
                .and()
                .oauth2Login().loginPage("/login").permitAll().defaultSuccessUrl("/auth/success")
                .and()
                .logout()
                .invalidateHttpSession(true)
                .clearAuthentication(true)
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/login?logout")
                .permitAll();

//        .and().logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
//                .logoutSuccessUrl("/").deleteCookies("JSESSIONID");

//        http.csrf().disable()
//                .authorizeRequests()
//                .antMatchers("/admin/**").hasAuthority(Role.ADMIN.toString())
//                .anyRequest().permitAll();


//                .and()
//                .formLogin().loginPage("/loginPage")
//                .and()
//                .oauth2Login().loginPage("/loginPage")
//                .defaultSuccessUrl("/social/success", true).failureUrl("/loginPage?socialError")
//                .and()
//                .logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
//                .logoutSuccessUrl("/loginPage?logout").deleteCookies("JSESSIONID")
//                .invalidateHttpSession(true)
//                .and()
//                .exceptionHandling().accessDeniedPage("/resources/html/errors/403.html");
    }
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        super.configure(auth);
            auth.userDetailsService(userDetailsService);
//        auth.inMemoryAuthentication()
//                .withUser("chnumarks@gmail.com").password("p").roles("Student")
//                .and()
//                .withUser("manager").password(BCrypt.hashpw("password",BCrypt.gensalt())).roles("MANAGER");

    }




    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder(10);
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }


}
